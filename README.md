# Eternal Legion Website #

[eternallegion.club](https://eternallegion.club)

_This personal project was made for the **Free Company** that I belong to in the **critically acclaimed MMORPG Final Fantasy XIV** developed by **Square Enix**. **This website is in no way affiliated with Square Enix**. This website is a blogging website made using the MERN stack, along with many other npm libraries._

Blogging websites in general are a great way to learn the complexities that come along with the backend and frontend logic. For example, the logic of deleting or editing an arleady existing post, for checking if post already exists, or simply integrating libraries for uploading multiple images or blog-styled text input boxes can be trickier than it may seem to the untrained eye.

As the access to posting / editing / deleting a blog would not be accessible to everyone except those who have the credentials, this README aims at depicting those very processes. 


## Libraries Used ##

**Frontend Libraries:**

 * [react](https://www.npmjs.com/package/react)
 * [react-router-dom](https://www.npmjs.com/package/react-router-dom)
 * [axios](https://www.npmjs.com/package/axios)
 * [bootstrap](https://getbootstrap.com/)
 * [font-awesome](https://fontawesome.com/)
 * [react-drag-drop-files](https://www.npmjs.com/package/react-drag-drop-files)
 * [react-quill](https://www.npmjs.com/package/react-quill)
 * [react-modall](https://www.npmjs.com/package/react-modal)

 **Backend Libraries:**

 * [cors](https://www.npmjs.com/package/cors)
 * [dotenv](https://www.npmjs.com/package/dotenv)
 * [express](https://www.npmjs.com/package/express)
 * [mongoose](https://www.npmjs.com/package/mongoose)
 * [nodemon](https://www.npmjs.com/package/nodemon) 


 ## Authentication ##

 Authentication process uses a post call using Axios to the backend that carries the credentials. This is a better way of doing it rather than getting all user information and then checking if the credentials matches any. Checking the match on middleware and then returning a status along with the user if authenticated ensures that there is no data leak. Once the user has logged in successfully, the login session is stored. This makes sure that login persists. Certain pages can only be accessed after checking that the user is authenticated. Certain logics to keep in mind were:

 * **If user has not logged in, and tries to acccess these pages using URL, user is returned to the login page.**
 * **If a fake token has been made with wrong credentials, the token is deleted on trying to access one of these pages, and the user is returned to the login page.**
 * **If user already has a login session, and goes to the login page, user is sent to the dashboard.**

**Login Page:**

<img src="https://i.imgur.com/3ocs4OO.png"/>

**Dashboard:**

<img src="https://i.imgur.com/HeGiqTC.jpg"/>


## Posts ##

 The authorized user can post a guide on one of the categories assigned at a time, an event and an album. The post appears on the dashboard where they can edit or delete it. The posting process keeps certain logics in consideration:

 * **If all the fields were properly filled during making of a post.**
 * **If the title exists already.**
 * **Making the right backend calls for making post. Do not want albums to end up as guides.**
 * **Changes made while editing the post.**
 * **Loading the right posts using the id it is saved with in MongoDB.**

**Guides Post Page:**

<img src="https://i.imgur.com/k5Jubyo.png"/>

**Albums Post Page:**

<img src="https://i.imgur.com/vvsAhAU.png">
