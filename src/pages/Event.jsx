import React, { useState, useEffect } from "react";
import "./Event.css";

function Event(props) {
  const [id, setID] = useState("");
  const [title, setTitle] = useState("");
  const [para, setPara] = useState("");
  const [image, setImage] = useState("");

  // When page is loaded
  useEffect(() => {
    // Setting URL as ID
    const url = window.location.href.split("/");
    while (url[url.length - 1] === "") {
      url.pop();
    }
    const lasturl = url[url.length - 1];
    setID(lasturl);
    console.log(lasturl);

    // Getting all Events, finding the event with this id
    props
      .getevents()
      .then((response) => {
        response.map((event) => {
          if (event._id === lasturl) {
            console.log("yes");
            setTitle(event.title);
            setPara(event.para);
            setImage(event.image);
            document.title = event.title + " | Eternal Legion";
          }
        });
      })
      .catch((err) => {});
  }, []);

  // Displaying Page
  return (
    <div id="event-div">
      <img className="thumbnail mb-4" src={image} alt="Thumbnail"></img>
      <h1 className="title px-3 mb-4 text-danger">{title}</h1>
      <hr className="divider mx-3 bg-danger"></hr>
      <div className="para">
      <div className="px-3" dangerouslySetInnerHTML={{ __html: para }} />
      </div>
    </div>
  );
}

export default Event;
