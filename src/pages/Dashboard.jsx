import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Axios from "axios";
import "./Dashboard.css";
import DashboardPostCard from "../components/DashboardPostCard";

function Dashboard(props) {
  const navigate = useNavigate();
  const [user, setUser] = useState("");
  const [name, setName] = useState("");
  const [guides, setGuides] = useState([]);
  const [events, setEvents] = useState([]);
  const [albums, setAlbums] = useState([]);

  useEffect(() => {
    // Title
    document.title = "Dashboard | Eternal Legion";

    // If Token Exists, Or You Go To Login Page
    try {
      const token = JSON.parse(localStorage.getItem("MyUser"));
      props
        .auth(token.username, token.password)
        .then((response) => {
          setUser(token.username);
          setName(token.name);
        })
        .catch((response) => {
          console.log("Error authenticate token, deleting.");
          localStorage.removeItem("MyUser");
          navigate("/login");
        });
    } catch (err) {
      navigate("/login");
    }

    // Getting All Guides
    props
      .getguides()
      .then((response) => setGuides(response))
      .catch((err) => console.log(err));

    // Getting All Events
    props
      .getevents()
      .then((response) => setEvents(response))
      .catch((err) => console.log(err));

    // Get All Albums
    props
      .getalbums()
      .then((response) => setAlbums(response))
      .catch((err) => console.log(err));
  }, []);

  // Editing Post link
  function EditPost(type, id) {
    return navigate("/editpost/" + type + "/" + id);
  }

  // Deleting Guide
  function DeletePost(type, id) {
    switch (type) {
      case "guides":
        props
          .deleteguide(id)
          .then((response) => {
            alert(response);
            window.location.reload();
          })
          .catch((err) => alert(err));
        window.location.reload();
        break;
      case "events":
        props
          .deleteevent(id)
          .then((response) => {
            alert(response);
            window.location.reload();
          })
          .catch((err) => alert(err));

        break;
      case "albums":
        props
          .deletealbum(id)
          .then((response) => {
            alert(response);
            window.location.reload();
          })
          .catch((err) => alert(err));
        window.location.reload();
        break;
      default:
    }
  }

  // Displaying Posts
  function DisplayPosts(posts, type) {
    return (
      <div key={posts._id} className="col-lg-3 col-sm-6 col-md-6">
        <DashboardPostCard post={posts} type={type} editpost={EditPost} deletepost={DeletePost} />
      </div>
    );
  }

  return (
    <div id="dashboard-div" className="text-light">
      <div className="header d-block">
        <h1>Username: {user}</h1>
        <h1>Name: {name}</h1>
        <h1>Guides: {guides.length}</h1>
        <h1>Events: {events.length}</h1>
        <h1>Albums: {albums.length}</h1>
      </div>
      <div className="new-posts">
        <button className="new-guide btn btn-outline-light mx-3" onClick={() => navigate("/post")}>
          New Guide
        </button>
        <button className="new-event btn btn-outline-light mx-3" onClick={() => navigate("/postevent")}>
          New Event
        </button>
        <button className="new-album btn btn-outline-light mx-3" onClick={() => navigate("/postalbum")}>
          New Album
        </button>
      </div>
      <hr />
      <div className="guides">
        <h1>Guides</h1>
        <hr className="guides-hr" />
        <div className="container-fluid">
          <div className="row">{guides.map((arr) => DisplayPosts(arr, "guides"))}</div>
        </div>
      </div>
      <div className="events">
        <h1>Events</h1>
        <hr className="events-hr" />
        <div className="container-fluid">
          <div className="row">{events.map((arr) => DisplayPosts(arr, "events"))}</div>
        </div>
      </div>
      <div className="albums">
        <h1>Albums</h1>
        <hr className="albums-hr" />
        <div className="container-fluid">
          <div className="row">{albums.map((arr) => DisplayPosts(arr, "albums"))}</div>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
