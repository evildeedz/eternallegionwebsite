import React, { useState, useEffect } from "react";
import Axios from "axios";
import GuidesCard from "../components/GuidesCard";
import "./Guidespage.css";

function Guidespage(props) {
  // States & Variables
  const [id, setId] = useState("");
  const [guides, setGuides] = useState([]);
  const [title, setTitle] = useState("");

  // Get Last Url
  function getLastURL() {
    const url = window.location.href.split("/");
    while (url[url.length - 1] === "") {
      url.pop();
    }
    const lasturl = url[url.length - 1];
    return lasturl;
  }

  // On Start
  function onStart() {
    // Getting Which Url We At
    const lasturl = getLastURL();
    setId(lasturl);

    // Title
    const spliturl = lasturl.split("_");
    let str = "";
    for (let i = 0; i < spliturl.length; i++) {
      str += spliturl[i].charAt(0).toUpperCase() + spliturl[i].slice(1).toLowerCase() + " ";
    }
    document.title = str + " | Eternal Legion";
    setTitle(str);

    // Getting Posts
    props
      .getguides()
      .then((response) => {
        setGuides(response);
      })
      .catch((err) => {
        alert(err);
      });
  }

  // On Page Load
  useEffect(() => {
    setInterval(() => {
      const lasturl = getLastURL();
      if (id != lasturl) {
        onStart();
      }
    }, 1000);
  }, []);

  // Making Cards
  function guideDisplay(guide) {
    if (id === guide.guide) {
      return (
        <div key={guide._id} className="col-lg-4 col-md-6 mb-5">
          <GuidesCard title={guide.title} thumbnail={guide.image} author={guide.author} id={guide._id} />
        </div>
      );
    }
  }

  return (
    <div id="guides-page-div">
      <div className="guides-cards">
        <h1 className="title text-warning">{title}</h1>
        <hr className="title-divider bg-warning" />
        <div className="row">{guides.map(guideDisplay)}</div>
      </div>
    </div>
  );
}

export default Guidespage;
