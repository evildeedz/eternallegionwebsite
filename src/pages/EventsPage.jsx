import React, { useState, useEffect } from "react";
import "./EventsPage.css";
import EventsCard from "../components/EventsCard";

function Eventspage(props) {
  // States & Variables
  const [events, setEvents] = useState([]);

  // On Page Load
  useEffect(() => {
    // Title
    document.title = "Events | Eternal Legion";
    // Getting Events
    props
      .getevents()
      .then((response) => {
        setEvents(response);
      })
      .catch((err) => {
        alert(err);
      });
  }, []);

  // Making Cards
  function eventDisplay(event) {
    return (
      <div key={event._id} className="col-lg-4 col-md-6 mb-5">
        <EventsCard title={event.title} thumbnail={event.image} id={event._id} />
      </div>
    );
  }

  return (
    <div id="events-page-div">
      <div className="events-cards">
        <h1 className="title text-warning">FC Events</h1>
        <hr className="title-divider bg-warning" />
        <div className="row">{events.map(eventDisplay)}</div>
      </div>
    </div>
  );
}

export default Eventspage;
