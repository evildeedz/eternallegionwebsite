import React, { useState, useEffect } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { useNavigate } from "react-router-dom";
import "./EventPostPage.css";
import { FileUploader } from "react-drag-drop-files";

function EventsPost(props) {
  const [title, setTitle] = useState("");
  const [para, setPara] = useState("");
  const [image, setImage] = useState("");
  const navigate = useNavigate();
  const fileTypes = ["JPG", "PNG"];
  const [id, setID] = useState("");

  // Setting Guide On Startup
  function setEventsOnStartup() {
    // Setting URL as ID
    const url = window.location.href.split("/");
    while (url[url.length - 1] === "") {
      url.pop();
    }
    const lasturl = url[url.length - 1];
    setID(lasturl);

    // Getting all Guides, finding the guide with this id
    props
      .getevents()
      .then((response) => {
        response.map((arr) => {
          if (arr._id === lasturl) {
            setTitle(arr.title);
            setPara(arr.para);
            setImage(arr.image);
          }
        });
      })
      .catch((err) => {});
  }

  // On Page Load
  useEffect(() => {
    // If Token Exists, Or You Go To Login Page
    try {
      document.title = "Post Event | Eternal Legion";
      const token = JSON.parse(localStorage.getItem("MyUser"));
      props
        .auth(token.username, token.password)
        .then((response) => {
          setEventsOnStartup();
        })
        .catch((response) => {
          console.log("Error authenticate token, deleting.");
          localStorage.removeItem("MyUser");
          navigate("/login");
        });
    } catch (err) {
      navigate("/login");
    }
  }, []);

  // Database Logic
  function submitHandler() {
    // Checking Empty Title
    if (title === "") {
      alert("Please enter Title.");
      return;
    }
    // Checking Empty Para
    if (para === "" || para === "<p><br></p>" || para === "<p></p>") {
      alert("Invalid Para.");
      return;
    }

    // Checking Invalid Image
    if (image === "") {
      alert("Please upload a Thumbnail.");
      return;
    }

    // Uploading Post
    props
      .updateevent(id, title, para, image)
      .then((response) => {
        return alert(response);
      })
      .catch((err) => {
        return alert(err);
      });
  }

  //Editing Toolbar Box
  const modules = {
    toolbar: [[{ header: [1, 2, 3, 4, 5, false] }], ["bold", "italic", "underline", "strike", "blockquote"], [{ align: "" }, { align: "center" }, { align: "right" }, { align: "justify" }], [{ list: "ordered" }, { list: "bullet" }, { indent: "-1" }, { indent: "+1" }], ["link", "image"], ["clean"]],
  };

  return (
    <div id="eventpost-page-div">
      <div>
        {/* Title & Para */}
        <input
          className="title my-3"
          type="text"
          placeholder="Title"
          onChange={(event) => {
            setTitle(event.target.value);
          }}
          value={title}
        />
        <br />
        <ReactQuill theme="snow" modules={modules} className="para bg-light mx-auto mb-5" onChange={setPara} value={para} placeholder="Enter Text" />
        <br />
        {/* Thumbnail */}
        <center>
          <FileUploader
            className="mt-5 mb-3"
            types={fileTypes}
            name="thumbnail"
            handleChange={(file) => {
              // Converting to base64
              const reader = new FileReader();
              reader.readAsDataURL(file);
              reader.onload = () => {
                setImage(reader.result);
              };
            }}
          />
        </center>
        <h3 className="text-warning my-3">Thumbnail Preview</h3>
        <img className="preview-img" src={image}></img>
        <br />
        {/* Submit Button */}
        <button className="btn btn-outline-light px-4 py-2 mt-4" onClick={submitHandler}>
          Submit
        </button>
      </div>
    </div>
  );
}

export default EventsPost;
