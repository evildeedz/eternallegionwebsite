import React, { useState, useEffect } from "react";
import "./AlbumsPage.css";
import AlbumsCard from "../components/AlbumsCard";
import AlbumModal from "../components/AlbumModal";

function AlbumsPage(props) {
  // States & Variables
  const [albums, setAlbums] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const [images, setImages] = useState([]);

  // On Page Load
  useEffect(() => {
    // Title
    document.title = "Albums | Eternal Legion";
    // Getting Albums
    props
      .getalbums()
      .then((response) => {
        setAlbums(response);
      })
      .catch((err) => {
        alert(err);
      });
  }, []);

  // On Album Click
  function onAlbumClick(albumImages) {
    setImages(albumImages);
    isOpenHandler();
  }

  // Setting Opposite of isOpen Value
  function isOpenHandler() {
    setIsOpen(prevItem=> !prevItem);
  }

  // Making Cards
  function albumDisplay(album) {
    return (
      <div key={album._id} className="col-lg-4 col-md-6 mb-5">
        <AlbumsCard album={album} onclick={onAlbumClick} />
      </div>
    );
  }

  return (
    <div id="albums-page-div">
      <div className="albums-cards">
        <h1 className="title text-warning">FC Memories</h1>
        <hr className="title-divider bg-warning" />
        <div className="row">{albums.map(albumDisplay)}</div>
      </div>
      { isOpen && <AlbumModal className="mt-5" images={images} isOpen={isOpen} closeHandler={isOpenHandler}/>}
    </div>
  );
}

export default AlbumsPage;
