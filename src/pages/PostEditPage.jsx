import React, { useState, useEffect } from "react";
import Axios from "axios";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { useNavigate } from "react-router-dom";
import "./Postpage.css";
import { FileUploader } from "react-drag-drop-files";

function Post(props) {
  const fileTypes = ["JPG", "PNG"];
  const [title, setTitle] = useState("");
  const [para, setPara] = useState("");
  const [guide, setGuide] = useState("Choose Category");
  const [guideName, setGuideName] = useState("Choose Category");
  const [image, setImage] = useState("");
  const [id, setID] = useState("");
  const navigate = useNavigate();

  // Setting Guide On Startup
  function setGuideOnStartup() {
    // Setting URL as ID
    const url = window.location.href.split("/");
    while (url[url.length - 1] === "") {
      url.pop();
    }
    const lasturl = url[url.length - 1];
    setID(lasturl);

    // Getting all Guides, finding the guide with this id
    props
      .getguides()
      .then((response) => {
        response.map((arr) => {
          if (arr._id === lasturl) {
            setTitle(arr.title);
            setPara(arr.para);
            setImage(arr.image);
            setGuide(arr.guide);
            // Setting Guide Name
            const guideNameSplit = arr.guide.split("_");
            let str = "";
            for (let i = 0; i < guideNameSplit.length; i++) {
              str += guideNameSplit[i].charAt(0).toUpperCase() + guideNameSplit[i].slice(1).toLowerCase() + " ";
            }
            setGuideName(str);
          }
        });
      })
      .catch((err) => {});
  }

  // On Page Load
  useEffect(() => {
    // If Token Exists, Or You Go To Login Page
    try {
      document.title = "Edit Guide | Eternal Legion";
      const token = JSON.parse(localStorage.getItem("MyUser"));
      props
        .auth(token.username, token.password)
        .then((response) => {
          setGuideOnStartup();
        })
        .catch((response) => {
          console.log("Error authenticate token, deleting.");
          localStorage.removeItem("MyUser");
          navigate("/login");
        });
    } catch (err) {
      navigate("/login");
    }
  }, []);

  // Database Logic
  function submitHandler() {
    // Checking Empty Title
    if (title === "") {
      alert("Please enter Title.");
      return;
    }
    // Checking Empty Para
    if (para === "" || para === "<p><br></p>" || para === "<p></p>") {
      alert("Invalid Para.");
      return;
    }
    // Checking Invalid Guide
    if (guide === "Choose Category") {
      alert("Please choose a Category.");
      return;
    }
    // Checking Invalid Image
    if (image === "") {
      alert("Please upload a Thumbnail.");
      return;
    }

    // Uploading Post
    props
      .updateguide(id, title, para, guide, image)
      .then((response) => {
        return alert(response);
      })
      .catch((err) => {
        return alert(err);
      });
  }

  //Editing Toolbar Box
  const modules = {
    toolbar: [[{ header: [1, 2, 3, 4, 5, false] }], ["bold", "italic", "underline", "strike", "blockquote"], [{ align: "" }, { align: "center" }, { align: "right" }, { align: "justify" }], [{ list: "ordered" }, { list: "bullet" }, { indent: "-1" }, { indent: "+1" }], ["link", "image"], ["clean"]],
  };

  return (
    <div id="post-page-div">
      <div>
        {/* Title & Para */}
        <input
          className="title my-3"
          type="text"
          placeholder="Title"
          onChange={(event) => {
            setTitle(event.target.value);
          }}
          value={title}
        />
        <br />
        <ReactQuill theme="snow" modules={modules} className="para bg-light mx-auto" onChange={setPara} placeholder="Enter Text" value={para} />
        <br />
        {/* Guides */}
        <div className="dropdown mt-5">
          <button className="btn btn-secondary dropdown-toggle" type="button" id="categoryDropdown" data-bs-toggle="dropdown" aria-expanded="false">
            {guideName}
          </button>
          <ul className="dropdown-menu bg-light" aria-labelledby="categoryDropdown">
            <li>
              <a
                className="dropdown-item"
                onClick={() => {
                  setGuideName("Beginners Guide");
                  setGuide("beginners_guide");
                }}
              >
                Beginners Guide
              </a>
            </li>
            <li>
              <a className="dropdown-item">Dungeons</a>
              <ul className="dropdown-submenu">
                <li>
                  <a
                    className="dropdown-item"
                    onClick={() => {
                      setGuideName("A Realm Reborn");
                      setGuide("a_realm_reborn");
                    }}
                  >
                    A Realm Reborn
                  </a>
                </li>
                <li>
                  <a
                    className="dropdown-item"
                    onClick={() => {
                      setGuideName("Heavensward");
                      setGuide("heavensward");
                    }}
                  >
                    Heavensward
                  </a>
                </li>
                <li>
                  <a
                    className="dropdown-item"
                    onClick={() => {
                      setGuideName("Stormblood");
                      setGuide("stormblood");
                    }}
                  >
                    Stormblood
                  </a>
                </li>
                <li>
                  <a
                    className="dropdown-item"
                    onClick={() => {
                      setGuideName("Shadowbringers");
                      setGuide("shadowbringers");
                    }}
                  >
                    Shadowbringers
                  </a>
                </li>
                <li>
                  <a
                    className="dropdown-item"
                    onClick={() => {
                      setGuideName("Endwalker");
                      setGuide("endwalker");
                    }}
                  >
                    Endwalker
                  </a>
                </li>
              </ul>
            </li>
            <li>
              <a
                className="dropdown-item"
                onClick={() => {
                  setGuideName("Gathering");
                  setGuide("gathering");
                }}
              >
                Gathering
              </a>
            </li>
            <li>
              <a
                className="dropdown-item"
                onClick={() => {
                  setGuideName("Crafting");
                  setGuide("crafting");
                }}
              >
                Crafting
              </a>
            </li>
            <li>
              <a
                className="dropdown-item"
                onClick={() => {
                  setGuideName("Secret Stuff");
                  setGuide("secret_stuff");
                }}
              >
                Secret Stuff
              </a>
            </li>
          </ul>
        </div>
        <br />
        {/* Thumbnail */}
        <center>
          <FileUploader
            className="mt-5 mb-3"
            types={fileTypes}
            name="thumbnail"
            handleChange={(file) => {
              // Converting to base64
              const reader = new FileReader();
              reader.readAsDataURL(file);
              reader.onload = () => {
                setImage(reader.result);
              };
            }}
          />
        </center>
        <h3 className="text-warning my-3">Thumbnail Preview</h3>
        <img className="preview-img" src={image}></img>
        <br />
        {/* Submit Button */}
        <button className="btn btn-outline-light px-4 py-2 mt-4" onClick={submitHandler}>
          Submit
        </button>
      </div>
    </div>
  );
}

export default Post;
