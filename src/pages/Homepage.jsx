import React, { useEffect } from "react";
import headImage from "../images/2.gif";
import "./Homepage.css";
import CompanyPerks from "../components/CompanyPerks";
import HomepageGuideBox from "../components/HomepageGuideBox";
import HomepageGuideBoxList from "../lists/HomepageGuideBoxList";
import CompanyPerksList from "../lists/CompanyPerksList";
import Footer from "../components/Footer";

function Homepage() {
  useEffect(() => {
    document.title = "Home | Eternal Legion";
  }, []);

  return (
    <div id="home-page-div">
      {/* Header Image */}
      <div className="head-img-div bg-dark">
        <img className="head-img" src={headImage}></img>
      </div>
      {/* Company Perks */}
      <div className="features-div">
        <h1 className="features-title text-warning">Free Company Perks</h1>
        <hr className="features-divider bg-warning"></hr>
        <center>
          <div className="features row">
            {CompanyPerksList.map((arr) => (
              <div key={arr.id} className="col-lg-4 col-md-12 col-sm-12">
                <CompanyPerks title={arr.title} text={arr.text} />
              </div>
            ))}
          </div>
        </center>
      </div>
      {/* Guides */}
      <div className="guides bg-light">
        {HomepageGuideBoxList.map((arr) => (
          <HomepageGuideBox key={arr.id} classname={arr.classname} buttontext={arr.buttontext} src={arr.src} title={arr.title} text={arr.text} link={arr.link}/>
        ))}
      </div>
      {/* Discord Us */}
      <div className="discord-div">
        <img src="https://i.imgur.com/KoFaxvo.png"></img>
        <div className="discord">
          <h1 className="text-warning">Join Our Discord Today!</h1>
          <a href="https://discord.gg/sDRpStYdw4" target="_blank">
            <button class="btn btn-outline-warning px-4 py-2 mt-4">
              <strong>
                <i class="fa-brands fa-discord"></i> Discord
              </strong>
            </button>
          </a>
        </div>
      </div>
    </div>
  );
}

export default Homepage;
