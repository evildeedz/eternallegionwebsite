import { Title } from "@mui/icons-material";
import Axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./Loginpage.css";

function Loginpage(props) {
  // States & Variables
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  // When Page Loads
  useEffect(() => {
    document.title = "Login";
    // checking if user  has already logged in
    try {
      const token = JSON.parse(localStorage.getItem("MyUser"));
      props
        .auth(token.username, token.password)
        .then((response) => {
          navigate("/dashboard");
        })
        .catch((response) => {
          console.log("Error authenticate token, deleting.");
          localStorage.removeItem("MyUser");
        });
    } catch (err) {}
  }, []);

  // Login Handler
  function submitHandler() {
    // Checking Empty Username
    if (username === "") {
      alert("Invalid Username.");
      return;
    }
    // Checking Empty Passwords
    if (password === "") {
      alert("Invalid Password.");
      return;
    }
    // Authenticating
    props
      .auth(username, password)
      .then((response) => {
        const user = response.user;
        localStorage.setItem("MyUser", JSON.stringify(user));
        navigate("/dashboard");
      })
      .catch((response) => {
        return alert("Wrong Credentials!");
      });
  }

  return (
    <div id="login-page-div">
      <h1 className="text-light login-title mb-4">Login</h1>
      {/* Username & Password */}
      <input
        className="my-3 mt-4"
        type="text"
        placeholder="Username"
        onChange={(event) => {
          setUsername(event.target.value);
        }}
        value={username}
      />
      <br />
      <input
        className="my-3"
        type="password"
        placeholder="Password"
        onChange={(event) => {
          setPassword(event.target.value);
        }}
        value={password}
      />
      <br />
      {/* Submit Button */}
      <button className="btn btn-outline-light px-4 py-2 mt-3" onClick={submitHandler}>
        Submit
      </button>
    </div>
  );
}

export default Loginpage;
