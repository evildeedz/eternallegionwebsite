import React, { useState, useEffect } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { useNavigate } from "react-router-dom";
import { FileUploader } from "react-drag-drop-files";
import "./AlbumPostPage.css";

function AlbumsPost(props) {
  const [title, setTitle] = useState("");
  const [image, setImage] = useState([]);
  const [src, setSrc] = useState("");
  const fileTypes = ["JPG", "PNG", "GIF"];

  const navigate = useNavigate();

  // On Page Load
  useEffect(() => {
    // If Token Exists, Or You Go To Login Page
    try {
      document.title = "Post Album | Eternal Legion";
      const token = JSON.parse(localStorage.getItem("MyUser"));
      props
        .auth(token.username, token.password)
        .then((response) => {})
        .catch((response) => {
          console.log("Error authenticate token, deleting.");
          localStorage.removeItem("MyUser");
          navigate("/login");
        });
    } catch (err) {
      navigate("/login");
    }
  }, []);

  // Database Logic
  function submitHandler() {
    // Checking Empty Title
    if (title === "") {
      alert("Please enter Title.");
      return;
    }

    // Checking Invalid Image
    if (image.length <= 0) {
      alert("Please upload a Thumbnail.");
      return;
    }

    // Uploading Post
    props
      .postalbum(title, image)
      .then((response) => {
        return alert(response);
      })
      .catch((err) => {
        return alert(err);
      });
  }

  //Editing Toolbar Box
  const modules = {
    toolbar: [[{ header: [1, 2, 3, 4, 5, false] }], ["bold", "italic", "underline", "strike", "blockquote"], [{ align: "" }, { align: "center" }, { align: "right" }, { align: "justify" }], [{ list: "ordered" }, { list: "bullet" }, { indent: "-1" }, { indent: "+1" }], ["link", "image"], ["clean"]],
  };

  return (
    <div id="albumpost-page-div">
      <div>
        {/* Thumbnail */}
        <center>
          <FileUploader
            className="mt-5 mb-3"
            multiple={true}
            types={fileTypes}
            name="thumbnail"
            handleChange={(e) => {
              Array.from(e).forEach((file) => {
                // Converting to base64
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = () => {
                  setImage((prevImages) => [...prevImages, reader.result]);
                };
              });
            }}
          />
        </center>
        <br />
        <h3 className="text-light">- OR -</h3>
        <input className="img-src my-3 py-2 px-2" type="text" value={src} placeholder="Add Image Source" onChange={(event) => setSrc(event.target.value)} />
        <button className="submit-img-src btn btn-outline-light py-1" onClick={() => {
          setImage((prevImages)=> [...prevImages, src]);
          setSrc("");
        }}>
          +
        </button>
        <br />
        <hr className="bg-light my-4" />
        <br />
        {/* Title */}
        <input
          className="title my-2 py-2 px-2"
          type="text"
          placeholder="Title"
          onChange={(album) => {
            setTitle(album.target.value);
          }}
          value={title}
        />
        <br />
        {/* Submit Button */}
        <button className="btn btn-outline-light px-4 py-2 mt-4" onClick={submitHandler}>
          Submit
        </button>
        {/* Displaying Images */}
        <div className="preview-div">
          <h2 className="text-warning my-3">Images Added</h2>
          {image.map((arr, index) => (
            <img className="preview-img mx-3 my-3" key={index} src={arr}></img>
          ))}
        </div>
      </div>
    </div>
  );
}

export default AlbumsPost;
