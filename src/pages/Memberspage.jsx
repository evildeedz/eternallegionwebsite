import { type } from "@testing-library/user-event/dist/type";
import Axios from "axios";
import React, { useEffect, useState } from "react";
import membersCover from "../images/membersCover.png";
import MembersCard from "../components/MemberCard";
import "./Memberspage.css";

function addThing(arr) {
  return (
    <div key={arr.ID} className="col-lg-3 col-md-4 col-sm-6 my-2 members-card">
      <MembersCard name={arr.Name} avatar={arr.Avatar} rank={arr.Rank} rankicon={arr.RankIcon} />
    </div>
  );
}

function Memberspage() {
  let fc;
  const [list, setList] = useState([]);
  const [loading, isLoading] = useState(true);

  useEffect(() => {
    document.title = "Members | Eternal legion";
    Axios.get("https://xivapi.com/freecompany/9230408911272647252?data=FCM").then((response) => {
      setList(response.data.FreeCompanyMembers);
      isLoading(false);
    });
  }, []);

  return (
    <div id="members-page-div">
      <div className="cover-img-header">
        <img className="cover-img mx-auto" src={membersCover} alt="FC Image"></img>
        <h1 className="members-title text-warning">Members</h1>
      </div>
      {loading && <h2 className="text-light">Loading..</h2>}
      <div className="row members-card-div pb-5">{list.map(addThing)}</div>
    </div>
  );
}

export default Memberspage;
