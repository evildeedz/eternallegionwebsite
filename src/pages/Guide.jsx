import React, { useState, useEffect } from "react";
import "./Guide.css";

function Guide(props) {
  const [id, setID] = useState("");
  const [author, setAuthor] = useState("");
  const [title, setTitle] = useState("");
  const [para, setPara] = useState("");
  const [image, setImage] = useState("");

  // When page is loaded
  useEffect(() => {
    // Setting URL as ID
    const url = window.location.href.split("/");
    while (url[url.length - 1] === "") {
      url.pop();
    }
    const lasturl = url[url.length - 1];
    setID(lasturl);
    console.log(lasturl);

    // Getting all Guides, finding the guide with this id
    props
      .getguides()
      .then((response) => {
        response.map((guide) => {
          if (guide._id === lasturl) {
            console.log("yes");
            setTitle(guide.title);
            setAuthor(guide.author);
            setPara(guide.para);
            setImage(guide.image);
            document.title = guide.title + " | Eternal Legion";
          }
        });
      })
      .catch((err) => {});
  }, []);

  // Displaying Page
  return (
    <div id="guide-div">
      <img className="thumbnail mb-4" src={image} alt="Thumbnail"></img>
      <h1 className="title px-3 mb-4 text-danger">{title}</h1>
      <h4 className="author text-secondary px-3"><em>By: {author}</em></h4>
      <hr className="divider mx-3 bg-danger"></hr>
      <div className="para">
      <div className="px-3" dangerouslySetInnerHTML={{ __html: para }} />
      </div>
    </div>
  );
}

export default Guide;
