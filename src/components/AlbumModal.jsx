import React from "react";
import ReactModal from "react-modal";

function AlbumModal({ images, isOpen, closeHandler }) {
  return (
    <div>
      <ReactModal className="album-modal-content" overlayClassName="album-modal-overlay" isOpen={isOpen} onRequestClose={closeHandler}>
        <div id="carouselAlbums" className="carousel slide" data-bs-ride="carousel">
          <div className="carousel-inner">
            {images.map((arr, index) => {
              if (index === 0) {
                return (
                  <div className="carousel-item active px-5">
                    <img src={arr} alt="..." />
                  </div>
                );
              } else {
                return (
                  <div className="carousel-item">
                    <img src={arr} alt="..." />
                  </div>
                );
              }
            })}
          </div>
          <button className="carousel-control carousel-control-prev" type="button" data-bs-target="#carouselAlbums" data-bs-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Previous</span>
          </button>
          <button className="carousel-control carousel-control-next" type="button" data-bs-target="#carouselAlbums" data-bs-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="visually-hidden">Next</span>
          </button>
        </div>
      </ReactModal>
    </div>
  );
}

export default AlbumModal;
