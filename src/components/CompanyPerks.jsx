import React from "react";

function CompanyPerks(props) {
  return (
    <div className="card my-5 mx-auto">
      <div className="card-body">
        <h3 className="card-title pt-5 pb-3 text-primary">{props.title}</h3>
        <p className="card-text px-3 pb-3 text-dark">{props.text}</p>
      </div>
    </div>
  );
}

export default CompanyPerks;
