import React from "react";
import { Link } from "react-router-dom";

function EventsCard(props) {
  return (
    <div className="card ms-auto">
      <Link className="link" to={props.id}>
        <img className="card-img-top" src={props.thumbnail} alt="Card image cap" />
        <div className="card-body">
          <h4 className="card-title text-primary">{props.title}</h4>
        </div>
      </Link>
    </div>
  );
}

export default EventsCard;
