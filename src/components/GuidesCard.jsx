import React from "react";
import { Link } from "react-router-dom";

function GuidesCard(props) {
  return (
    <div className="card ms-auto">
      <Link className="link" to={props.id}>
        <img className="card-img-top" src={props.thumbnail} alt="Card image cap" />
        <div className="card-body">
          <h4 className="card-title text-primary">{props.title}</h4>
          <hr/>
          <p className="card-text text-secondary">
            <em>{"By: " + props.author}</em>
          </p>
        </div>
      </Link>
    </div>
  );
}

export default GuidesCard;
