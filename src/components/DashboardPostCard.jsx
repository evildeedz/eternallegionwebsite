import React from "react";
import { Link, Navigate } from "react-router-dom";

function DashboardPostCard({ post, type, editpost, deletepost }) {
  return (
    <div className="card mx-auto my-5">
      <div className="card-body">
        <img className="card-title  mb-2" src={post.image}></img>
        <h1 className="card-text px-3 text-primary">{post.title}</h1>
        <button className="delete-btn btn btn-danger" onClick={() => deletepost(type, post._id)}>
          Delete
        </button>
        <button className="edit-btn btn btn-dark" onClick={() => editpost(type, post._id)}>
          Edit
        </button>
      </div>
    </div>
  );
}

export default DashboardPostCard;
