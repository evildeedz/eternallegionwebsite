import React from "react";

function MembersCard(props) {
  return (
    <div className="card">
      <img className="card-img-top mt-4" src={props.avatar} alt="Card image cap" />
      <div className="card-body">
        <h5 className="card-title">{props.name}</h5>
        <p className="card-text"><img className="mb-2" src={props.rankicon} alt="Rank Icon"/>{" " + props.rank}</p>
      </div>
    </div>
  );
}

export default MembersCard;
