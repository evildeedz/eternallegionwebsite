import React from "react";
import { Link } from "react-router-dom";
import "./Navbar.css";
import logo from "../images/logo.png";

function Navbar() {
  return (
    <div>
      <nav className="navbar pt-2 pb-1 navbar-expand-lg navbar-light bg-warning">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">
            <img className="navbar-brand-img pb-3" src={logo}></img>
            Eternal Legion
          </Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mb-2 mb-lg-0 ms-auto">
              <li className="nav-item">
                <Link className="nav-link active" aria-current="page" to="/">
                  Home
                </Link>
              </li>

              <li className="nav-item">
                <Link className="nav-link active" to="/members">
                  Members
                </Link>
              </li>

              {/* Guides */}
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle active" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Guides
                </a>
                <ul className="dropdown-menu bg-light" aria-labelledby="navbarDropdown">
                  <li>
                    <Link className="dropdown-item" to="/guides/beginners_guide">
                      Beginners Guide
                    </Link>
                  </li>
                  <li>
                    <a className="dropdown-item">Dungeons / Raids</a>
                    <ul className="dropdown-submenu">
                      <li>
                        <Link className="dropdown-item" to="/guides/a_realm_reborn">
                          A Realm Reborn
                        </Link>
                      </li>
                      <li>
                        <Link className="dropdown-item" to="/guides/heavensward">
                          Heavensward
                        </Link>
                      </li>
                      <li>
                        <Link className="dropdown-item" to="/guides/stormblood">
                          Stormblood
                        </Link>
                      </li>
                      <li>
                        <Link className="dropdown-item" to="/guides/shadowbringers">
                          Shadowbringers
                        </Link>
                      </li>
                      <li>
                        <Link className="dropdown-item" to="/guides/endwalker">
                          Endwalker
                        </Link>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <Link className="dropdown-item" to="/guides/gathering">
                      Gathering
                    </Link>
                  </li>
                  <li>
                    <Link className="dropdown-item" to="/guides/crafting">
                      Crafting
                    </Link>
                  </li>
                  <li>
                    <Link className="dropdown-item" to="/guides/secret_stuff">
                      Secret Stuff
                    </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item">
                <Link className="nav-link active" to="/events">
                  Events
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link active" to="/albums">
                  Albums
                </Link>
              </li>
              <li className="nav-item">
                <a className="nav-link active" target="_blank" href="https://na.finalfantasyxiv.com/lodestone/community_finder/03bed1e3dfc15d065c84c77d84920e161b161ffc/">
                  About Us
                </a>
              </li>
              <li className="nav-item">
                <a href="https://discord.gg/sDRpStYdw4" className="nav-link active" target="_blank">
                  Discord
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default Navbar;
