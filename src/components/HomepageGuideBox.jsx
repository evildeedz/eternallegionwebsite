import React from "react";
import { useNavigate } from "react-router-dom";

function HomepageGuideBox(props) {
  const navigate = useNavigate();

  return (
    <div className={props.classname}>
      <img className="mt-2" src={props.src} />
      <h1 className="text-light">{props.title}</h1>
      <p className="text-light">{props.text}</p>
      <button className="btn btn-outline-light px-3 py-2 mt-2" onClick={() => navigate(props.link)}>
        <strong>{props.buttontext}</strong>
      </button>
    </div>
  );
}

export default HomepageGuideBox;
