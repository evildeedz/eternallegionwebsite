import React from "react";

function AlbumsCard({ album, onclick }) {

  return (
    <div className="card mx-5" onClick={() => {
      onclick(album.image)
    }}>
      <div className="card-body">
        <img className="card-title text-primary mb-4" src={album.image[0]}></img>
        <hr />
        <h2 className="card-text py-2 px-3 text-primary">{album.title}</h2>
      </div>
    </div>
  );
}

export default AlbumsCard;
