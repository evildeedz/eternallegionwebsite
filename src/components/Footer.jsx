import React from "react";
import "./Footer.css";

function Footer() {
  return (
    <footer id="footer-div" className="bg-dark">
      {/* Above Div */}
      <div className="company-links">
        <h3>Free Company Links</h3>
        <div className="container mt-3">
          <div className="row">
            <div className="col-lg-4 col-sm-4 col-md-4">
              <p>
                -{" "}
                <a href="https://discord.gg/sDRpStYdw4" target="_blank">
                  Discord
                </a>
              </p>
            </div>
            <div className="col-lg-4 col-sm-4 col-md-4">
              <p>
                -{" "}
                <a href="https://na.finalfantasyxiv.com/lodestone/freecompany/9230408911272647252/" target="_blank">
                  FC Lodestone
                </a>
              </p>
            </div>
            <div className="col-lg-4 col-sm-4 col-md-4">
              <p>
                -{" "}
                <a href="https://na.finalfantasyxiv.com/lodestone/community_finder/03bed1e3dfc15d065c84c77d84920e161b161ffc/" target="_blank">
                  Apply To FC
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
      {/* Divider */}
      <hr className="footer-divider mx-5" />
      {/* Credits */}
      <div className="credit-div py-3">
        <p className="credit">
          Made by :
          <a href="https://gitlab.com/evildeedz" target="_blank" className="credit-link">
            {" "}
            EviL DeEdZ
          </a>
        </p>
      </div>
    </footer>
  );
}

export default Footer;
