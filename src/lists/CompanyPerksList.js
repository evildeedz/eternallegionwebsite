const list = [
  {
    id: 0,
    title: <i className="fa-solid fa-handshake-angle"></i>,
    text: "1. Helpful and friendly FC members, just ask for help in FC chat. Don't be shy!",
  },
  {
    id: 1,
    title: <i className="fa-solid fa-shield-cat"></i>,
    text: "2. Permanent uptime on FC buffs - 10% extra experience and 30% discount on teleportation fees.",
  },
  {
    id: 2,
    title: <i className="fa-solid fa-building"></i>,
    text: "3. Always available FC apartments for those who are interested in player housing.",
  },
  {
    id: 3,
    title: <i className="fa-solid fa-paw"></i>,
    text: "4. Chocobo stable for those who are interested in giving the best to their chocobos.",
  },
  {
    id: 4,
    title: <i className="fa-solid fa-sack-dollar"></i>,
    text: "5. FC Social Events with fun and lucrative prizes to be won!",
  },
  {
    id: 5,
    title: <i className="fa-solid fa-ban"></i>,
    text: "6. No FC tax. This FC will never ask you for gil or items.",
  },
];

export default list;
