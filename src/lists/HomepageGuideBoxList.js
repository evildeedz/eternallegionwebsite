const list = [
  {
    id: 0,
    classname: "beginners",
    src: "https://i.ytimg.com/vi/h542YbZuwkQ/maxresdefault.jpg",
    title: "Beginner Friendly",
    text: "We welcome beginners with open arms to join us in this easy-going and relaxed sanctuary. We love to help new players learn the game, and are always ready to help!",
    buttontext: "We Provide Beginners Guides Too!",
    link: "/guides/beginners_guide",
  },
  {
    id: 1,
    classname: "raiding",
    src: "https://storage.googleapis.com/gamebyte/2021/11/8bac43d8-ffxiv-raid.jpg",
    title: "Raids & Dungeons",
    text: "Although we are known for being casuals, we do have our own share of experiencing the most hardcore content game has to offer! We indulge in daily raiding and have some amazing guides on how to do them!",
    buttontext: "Check Them Out!",
    link: "guides/a_realm_reborn",
  },
  {
    id: 2,
    classname: "events",
    src: "https://i.imgur.com/OkOa8wh.png",
    title: "Events",
    text: "We love socializing and love doing fun stuff together with our members! We hold regular events in our Free Company and give out huge rewards for them. We wanna make sure you have a great day!",
    buttontext: "View Our Upcoming Events!",
    link: "/events",
  },
  {
    id: 3,
    classname: "albums",
    src: "https://i.imgur.com/P1myFqe.png",
    title: "Albums",
    text: "An album that contains all our memories! We love taking pictures, showcasing and flexing our glams and make the most hilarious poses! We would love to have  photo sessions with you as well ;)",
    buttontext: "Album Showcase",
    link: "/albums",
  },
];

export default list;
