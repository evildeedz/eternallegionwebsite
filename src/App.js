import React from "react";
import Homepage from "./pages/Homepage";
import Navbar from "./components/Navbar";
import Post from "./pages/Postpage";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Memberspage from "./pages/Memberspage";
import Loginpage from "./pages/Loginpage";
import Dashboard from "./pages/Dashboard";
import Guidespage from "./pages/Guidespage";
import EventPostPage from "./pages/EventPostPage";
import EventsPage from "./pages/EventsPage";
import AlbumPostPage from "./pages/AlbumPostPage";
import AlbumsPage from "./pages/AlbumsPage";
import Event from "./pages/Event";
import Guide from "./pages/Guide";
import Axios from "axios";
import PostEditPage from "./pages/PostEditPage";
import EventEditPage from "./pages/EventEditPage";
import Footer from "./components/Footer";
import "./App.css";

const path = process.env.REACT_APP_PATH;

function App() {
  // Authenticate User
  function auth(username, password) {
    return new Promise((resolve, reject) => {
      Axios.post(path + "/backend/users/login", {
        username,
        password,
      })
        .then((response) => {
          // if successful
          resolve({ auth: true, user: response.data.user });
        })
        .catch((err) => {
          // if not successful
          reject({ auth: false });
        });
    });
  }

  // Posting Guide
  function postGuide(title, para, guide, image, author) {
    return new Promise((resolve, reject) => {
      Axios.post(path + "/backend/posts/createPost", {
        title,
        para,
        guide,
        image,
        author,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err.response.data);
        });
    });
  }

  // Getting All Guides
  function getGuides() {
    return new Promise((resolve, reject) => {
      Axios.get(path + "/backend/posts/getGuides")
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err.response.data);
        });
    });
  }

  // Update Guide
  function updateGuide(id, title, para, guide, image) {
    return new Promise((resolve, reject) => {
      Axios.patch(path + "/backend/posts/editPost/" + id, { title, para, guide, image })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err.response.data);
        });
    });
  }

  // Delete Guide
  function deleteGuide(id) {
    return new Promise((resolve, reject) => {
      Axios.delete(path + "/backend/posts/deletePost/" + id)
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err.response.data);
        });
    });
  }

  // Posting Events
  function postEvent(title, para, image) {
    return new Promise((resolve, reject) => {
      Axios.post(path + "/backend/events/createEvent", {
        title,
        para,
        image,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err.response.data);
        });
    });
  }

  // Getting All Events
  function getEvents() {
    return new Promise((resolve, reject) => {
      Axios.get(path + "/backend/events/getEvents")
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err.response.data);
        });
    });
  }

  // Update Events
  function updateEvent(id, title, para, image) {
    return new Promise((resolve, reject) => {
      Axios.patch(path + "/backend/events/editEvent/" + id, { title, para, image })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err.response.data);
        });
    });
  }

  // Delete Event
  function deleteEvent(id) {
    return new Promise((resolve, reject) => {
      Axios.delete(path + "/backend/events/deleteEvent/" + id)
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err.response.data);
        });
    });
  }

  // Posting Albums
  function postAlbum(title, image) {
    return new Promise((resolve, reject) => {
      Axios.post(path + "/backend/albums/createAlbum", {
        title,
        image,
      })
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err.response.data);
        });
    });
  }

  // Getting All Albums
  function getAlbums() {
    return new Promise((resolve, reject) => {
      Axios.get(path + "/backend/albums/getAlbums")
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err.response.data);
        });
    });
  }

  // Delete Album
  function deleteAlbum(id) {
    return new Promise((resolve, reject) => {
      Axios.delete(path + "/backend/albums/deleteAlbum/" + id)
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err.response.data);
        });
    });
  }
  return (
    <div className="page-container">
      <div className="content-wrap">
        <BrowserRouter>
          <Navbar />
          <Routes>
            {/* Home Page  */}
            <Route exact path="/" element={<Homepage />} />
            {/* Posts Page */}
            <Route exact path="/post" element={<Post auth={auth} postguide={postGuide} />} />
            {/* Post Edit */}
            <Route exact path="/editpost/guides/:id" element={<PostEditPage auth={auth} getguides={getGuides} updateguide={updateGuide} />} />
            {/* Members Page */}
            <Route exact path="/members" element={<Memberspage />} />
            {/* Login Page */}
            <Route exact path="/login" element={<Loginpage auth={auth} />} />
            {/* Dashboard */}
            <Route exact path="/dashboard" element={<Dashboard auth={auth} getguides={getGuides} deleteguide={deleteGuide} getevents={getEvents} deleteevent={deleteEvent} getalbums={getAlbums} deletealbum={deleteAlbum} />} />
            {/* Guides Page */}
            <Route exact path="/guides/:id" element={<Guidespage getguides={getGuides} />} />
            {/* Guide */}
            <Route exact path="/guides/:category/:id" element={<Guide getguides={getGuides} />} />
            {/* Events Post Page */}
            <Route exact path="/postevent" element={<EventPostPage auth={auth} postevent={postEvent} />} />
            {/* Events Page */}
            <Route exact path="/events" element={<EventsPage getevents={getEvents} />} />
            {/* Event */}
            <Route exact path="/events/:id" element={<Event getevents={getEvents} />} />
            {/* Event Edit */}
            <Route exact path="/editpost/events/:id" element={<EventEditPage auth={auth} getevents={getEvents} updateevent={updateEvent} />} />
            {/* Albums Post Page */}
            <Route exact path="/postalbum" element={<AlbumPostPage auth={auth} postalbum={postAlbum} />} />
            {/* Albums Page */}
            <Route exact path="/albums" element={<AlbumsPage getalbums={getAlbums} />} />
          </Routes>
        </BrowserRouter>
      </div>
      <Footer />
    </div>
  );
}

export default App;
