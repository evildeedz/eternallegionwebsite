const mongoose = require("mongoose");

const AlbumSchema = mongoose.Schema({
    image: [{
        type: String,
        required: true
    }],
    title: {
        type: String,
        required: true,
        unique: true,
    }
});

module.exports = mongoose.model("albums", AlbumSchema);