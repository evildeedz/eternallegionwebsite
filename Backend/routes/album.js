const router = require("express").Router();
const AlbumModel = require("../models/Albums");

router.get("/getAlbums", (req, res) => {
  AlbumModel.find({}, (err, result) => {
    if (result) {
      res.status(200).json(result);
    } else {
      res.stats(400).json(err);
    }
  });
});

router.post("/createAlbum", async (req, res) => {
  try {
    AlbumModel.findOne({ title: req.body.title }, (err, album) => {
      if (album) {
        res.status(400).json("Album already exists. Try Changing Title.");
      } else {
        const album = req.body;
        const newAlbum = new AlbumModel(album);
        newAlbum.save();
        res.status(200).json("Album Created!");
      }
    });
  } catch (err) {}
});

router.patch("/editAlbum/:id", async (req, res) => {
  try {
    AlbumModel.findOneAndUpdate({ _id: req.params.id }, req.body, (err, doc) => {
      if (err) {
        res.status(400).json("Error: " + err);
      } else {
        res.status(200).json("Updated!");
      }
    });
  } catch (err) {}
});

router.delete("/deleteAlbum/:id", async (req, res) => {
  try {
    AlbumModel.findOneAndDelete({ _id: req.params.id }, (err, doc) => {
      if (err) {
        res.status(400).json("error: " + err);
      } else {
        res.status(200).json("Deleted!");
      }
    });
  } catch (err) {}
});

module.exports = router;
