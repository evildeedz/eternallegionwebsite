const router = require("express").Router();
const UserModel = require("../models/User");

router.post("/login", (req, res) => {
  UserModel.findOne({ username: req.body.username, password: req.body.password }, (err, user) => {
    if (user) {
      res.status(200).json({ message: "User Found", user: user });
    } else {
      res.status(400).json({ message: "User Not Found" });
    }
  });
});

module.exports = router;
