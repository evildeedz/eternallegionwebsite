const router = require("express").Router();
const EventModel = require("../models/Events");

router.get("/getEvents", (req, res) => {
  EventModel.find({}, (err, result) => {
    if (result) {
      res.status(200).json(result);
    } else {
      res.stats(400).json(err);
    }
  });
});

router.post("/createEvent", async (req, res) => {
  try {
    EventModel.findOne({ title: req.body.title }, (err, event) => {
      if (event) {
        res.status(400).json("Event already exists. Try Changing Title.");
      } else {
        const event = req.body;
        const newEvent = new EventModel(event);
        newEvent.save();
        res.status(200).json("Event Created!");
      }
    });
  } catch (err) {}
});

router.patch("/editEvent/:id", async (req, res) => {
  try {
    EventModel.findOneAndUpdate({ _id: req.params.id }, req.body, (err, doc) => {
      if (err) {
        res.status(400).json("Error: " + err);
      } else {
        res.status(200).json("Updated!");
      }
    });
  } catch (err) {}
});

router.delete("/deleteEvent/:id", async (req, res) => {
  try {
    EventModel.findOneAndDelete({ _id: req.params.id }, (err, doc) => {
      if (err) {
        res.status(400).json("error: " + err);
      } else {
        res.status(200).json("Deleted!");
      }
    });
  } catch (err) {}
});

module.exports = router;
