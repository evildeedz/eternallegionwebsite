const router = require("express").Router();
const PostModel = require("../models/Posts");

router.get("/getGuides", (req, res) => {
  PostModel.find({}, (err, result) => {
    if (result) {
      res.status(200).json(result);
    } else {
      res.stats(400).json(err);
    }
  });
});

router.post("/createPost", async (req, res) => {
  try {
    PostModel.findOne({ title: req.body.title }, (err, post) => {
      if (post) {
        res.status(400).json("Post already exists. Try Changing Title.");
      } else {
        const post = req.body;
        const newPost = new PostModel(post);
        newPost.save();
        res.status(200).json("Posted!");
      }
    });
  } catch (err) {}
});

router.patch("/editPost/:id", async (req, res) => {
  try {
    PostModel.findOneAndUpdate({ _id: req.params.id }, req.body, (err, doc) => {
      if (err) {
        res.status(400).json("Error: " + err);
      } else {
        res.status(200).json("Updated!");
      }
    });
  } catch (err) {}
});

router.delete("/deletePost/:id", async (req, res) => {
  try {
    PostModel.findOneAndDelete({ _id: req.params.id }, (err, doc) => {
      if (err) {
        res.status(400).json("error: " + err);
      } else {
        res.status(200).json("Deleted!");
      }
    });
  } catch (err) {}
});

module.exports = router;
