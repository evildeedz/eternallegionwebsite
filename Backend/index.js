const express = require("express");
const app = express();
const mongoose = require("mongoose");
require('dotenv').config();
const db = process.env.DB_CONNECT;
const port = process.env.PORT;
const ip = process.env.IP;
const auth = require('./routes/auth');
const post = require('./routes/post');
const event = require('./routes/event');
const album = require('./routes/album');

const cors = require("cors");

app.use(express.json({limit: '1000mb'}));
app.use(cors());

mongoose.connect(db, (er) => {
    if(er) {
        console.log(er);
    }
    else {
        console.log("Connected to database");
    }
});

app.use('/backend/users', auth);
app.use('/backend/posts', post);
app.use('/backend/events', event);
app.use('/backend/albums', album);

app.listen(port, ip,() => {
  console.log(`Example app listening on port ${port}`);
});
